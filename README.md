# dashboard

This project contains the dashboard used by business users to manually test the models deployed in TED AI environment.

The dashboard application has been implemented using gradio library.

## Requirements

* python 3.8+
* pip
* AWS credentials setup if you want to run it locally

## Setup

```shell
$ python3 -m venv venv
$ source venv/bin/activate
$ pip3 install -r requirements.txt
```

## Code
The configuration of the models is retrieved via AWS SSM (parameter store).
The dashboard is started on port 7860.

## Manual Testing
These commands must be executed from the root directory of the project.

```shell
$ AWS_PROFILE="xxx" python3 app/dashboard.py
```
Replace xxx by the AWS profile of the TED AI environment

## CI/CD
TED AI AWS environment contains three Amazon ECR repositories:
* ci-temporary-images: for all docker images pushed during CI setup for testing. These images are automatically deleted after 1 day
* sagemaker-classifiers: for all docker images of models meant to be used in production after the testing was completed
* ted-applications: for docker images of applications

This pipeline deploys in ci-temporary-images when in a feature branch or main. When a tag is created, the docker image is pushed to ted-applications.
