import os
from pathlib import Path
from typing import List

import gradio as gr
import requests
from requests import Response

API_URL = os.environ.get("LAWFULNESS_API_URL", "http://localhost:8080")

STATUS_COLUMNS = [
    'Filename',
    'Valid eForms notice',
    'Notice number',
    'Must be reviewed',
]
FLAGS_COLUMNS = [
    'Notice number',
    '"test" word',
    '"sale" word',
    'Incoherent text',
    'Forbidden country',
    'Non EU country + not funded by EU',
    'Private contracting body + not funded by EU',
    'Not whitelisted + international contracting body',
]


def display_tab():
    with gr.Tab("Lawfulness"):
        gr.Markdown("# <u>Lawfulness</u>")
        gr.Markdown("Only eForms notices in XML format are supported.")
        files = gr.File(file_count="multiple")
        run_button = gr.Button("Run lawfulness processing")
        status_table = gr.Dataframe(row_count=(3, "dynamic"),
                                    col_count=(len(STATUS_COLUMNS), "fixed"),
                                    headers=STATUS_COLUMNS,
                                    label="Status")
        flags_table = gr.Dataframe(row_count=(3, "dynamic"),
                                   col_count=(len(FLAGS_COLUMNS), "fixed"),
                                   headers=FLAGS_COLUMNS,
                                   label="Notices to review")
        show_raw_checkbox = gr.Checkbox(label="Show raw response")
        details_block = gr.JSON(label="Raw response", visible=False)
        run_button.click(_run, inputs=files, outputs=[status_table, flags_table, details_block])
        show_raw_checkbox.change(_show_raw, inputs=[show_raw_checkbox], outputs=[details_block])


def _run(files: list) -> List[list]:
    response = requests.post(f"{API_URL}/notices/lawfulness", files=[
        ("file", Path(file.name).read_bytes())
        for file in files
    ])
    return [_format_status(files, response), _format_flags(files, response), response.text]


def _format_status(files: list, response: Response):
    results = []
    for file, result in zip(files, response.json()):
        results.append([
            Path(file.name).name,
            bool(result),
            result['notice_id'] if result else "-",
            any(result['flags'].values()) if result else "-",
        ])
    return results


def _format_flags(files: list, response: Response):
    results = []
    for file, result in zip(files, response.json()):
        if not result or not any(result['flags'].values()):
            continue
        results.append([
            result['notice_id'],
            _beautify_boolean(result['flags']['has_test_word']),
            _beautify_boolean(result['flags']['has_sale_word']),
            _beautify_boolean(result['flags']['has_incoherent_text']),
            _beautify_boolean(result['flags']['is_forbidden_country']),
            _beautify_boolean(result['flags']['has_non_eu_country_and_not_funded_by_eu']),
            _beautify_boolean(result['flags']['is_contracting_body_private_and_not_funded_by_eu']),
            _beautify_boolean(result['flags']['has_not_whitelisted_international_contracting_body']),
        ])
    return results


def _beautify_boolean(boolean: bool) -> str:
    return "Yes" if boolean else ""


def _show_raw(is_shown: bool) -> gr.JSON:
    return gr.JSON(visible=is_shown)
